// use futures::IntoFuture;

use actix_web::{
    get, middleware, App, HttpServer,
};

#[get("/")]
fn no_params() -> &'static str {
    "Hello world!\r\n"
}

fn main() -> std::io::Result<()> {
    std::env::set_var("RUST_LOG", "actix_server=error,actix_web=error");
    env_logger::init();

    HttpServer::new(|| {
        App::new()
            //.wrap(middleware::Compress::default())
            .wrap(middleware::Logger::default())
            .service(no_params)
    })
    .bind("127.0.0.1:8080")?
    //.workers(4)
    .run()
}
