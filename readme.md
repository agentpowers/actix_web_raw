# MacBook Pro (Retina, 13-inch, Early 2015) | 2.7 GHz Intel Core i5 | 16 GB 1867 MHz DDR3
### wrk -d30s -c100 -t10 http://127.0.0.1:8080/
### Running 30s test @ http://127.0.0.1:8080/
###   10 threads and 100 connections
###   Thread Stats   Avg      Stdev     Max   +/- Stdev
###     Latency     2.11ms    2.74ms 105.17ms   99.20%
###     Req/Sec     5.06k   283.19     6.78k    66.20%
###   1511295 requests in 30.02s, 188.81MB read
### Requests/sec:  50347.69
### Transfer/sec:      6.29MB